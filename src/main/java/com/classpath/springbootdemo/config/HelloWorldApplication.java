package com.classpath.springbootdemo.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class HelloWorldApplication implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello world from Spring Boot 2.x !!");
    }
}